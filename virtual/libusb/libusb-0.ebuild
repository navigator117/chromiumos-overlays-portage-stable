# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/virtual/libusb/libusb-0.ebuild,v 1.11 2012/05/22 18:37:54 ssuominen Exp $

EAPI=2

DESCRIPTION="Virtual for libusb"
HOMEPAGE=""
SRC_URI=""

LICENSE=""
SLOT="0"
KEYWORDS="*"
IUSE=""

DEPEND=""
RDEPEND="|| ( >=dev-libs/libusb-compat-0.1.4 >=dev-libs/libusb-0.1.12-r7:0 >=sys-freebsd/freebsd-lib-8.0[usb] )"
