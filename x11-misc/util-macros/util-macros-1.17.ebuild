# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/x11-misc/util-macros/util-macros-1.17.ebuild,v 1.3 2012/05/04 15:36:40 chithanh Exp $

EAPI=4
inherit xorg-2

EGIT_REPO_URI="git://anongit.freedesktop.org/git/xorg/util/macros"
DESCRIPTION="X.Org autotools utility macros"

KEYWORDS="*"
IUSE=""

RDEPEND=""
DEPEND="${RDEPEND}"
